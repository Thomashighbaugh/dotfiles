#!/bin/bash 

sudo pacman -S --noconfirm emacs pandoc pandoc-citeproc pandoc-crossref texlive-bibtexextra texlive-bin texlive-core texlive-fontsextra texlive-formatsextra texlive-games texlive-humanities texlive-latexextra texlive-music texlive-pictures texlive-pstricks texlive-publishers texlive-science psutils perl-tk tlutils ed biber hunspell-en_US speech-dispatcher festival espeak-ng 
echo 'Finished Installing Emacs Packages'
