#!/bin/bash

echo 'Installing Beta Drivers from Nvidia'

yay -S --noconfirm --batchinstall --sudoloop nvidia-full-beta-all

echo 'Installing Driver Utilities, Settings and Extras'

yay -S --noconfirm --batchinstall --sudoloop  nvidia-settings-full-beta-all nvidia-utils-full-beta-all opencl-nvidia-full-beta-all



yay -S --noconfirm --batchinstall --sudoloop libvdpau cuda libxnvctrl nvidia-docker nvidia-xrun nvidia-container-runtime nvidia-container-toolkit 

echo 'Nvidia Installation is Complete'

