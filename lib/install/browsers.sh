#!/bin/bash 

sudo pacman -S  --noconfirm firefox-developer-edition falkon min browserpass chromium poppler-data links lynx w3m

yay -S --sudoloop --noconfirm --needed basilisk-bin firefox-adblock-plus ice-ssb browser-vacuum ffsend arch-firefox-search netsurf-git

yay -S --sudoloop --noconfirm --needed firefox-extension-trackmenot  firefox-extension-https-everywhere r 

yay -S --sudoloop --noconfirm --needed firefox-extension-temporary-containers firefox-extension-privacybadger firefox-ublock-origin

echo 'Finished Installing Browser Packages'
