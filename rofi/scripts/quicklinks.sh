#!/bin/bash



rofi_command="rofi -theme themes/quicklinks.rasi"

# Links
google=""
gphotos=""
twitter=""
github=""
reddit=""
youtube=""

# Variable passed to rofi
options="$google\n$gphotos\n$twitter\n$github\n$reddit\n$youtube"

chosen="$(echo -e "$options" | $rofi_command -p " Links |" -dmenu -selected-row 0)"
case $chosen in
    $google)
        firefox-developer-edition --new-tab https://www.google.com
        ;;
    $gphotos)
        firefox-developer-edition --new-tab https://photos.google.com
        ;;
    $twitter)
        firefox-developer-edition --new-tab https://www.twitter.com
        ;;
    $github)
        firefox-developer-edition --new-tab https://www.github.com
        ;;
    $reddit)
        firefox-developer-edition --new-tab https://www.reddit.com
        ;;
    $youtube)
        firefox-developer-edition --new-tab https://www.youtube.com
        ;;
esac

