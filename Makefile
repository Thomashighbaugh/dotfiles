.PHONY: help sh zsh bash rc uefi kitty zathura laptop neofetch ryzen awesome archive disk X tmux vim neovim rofi browsers docker dev emacs fonts git gtk android media netsec vm minikube postgresql pip yarn i3

help:
	@echo 'Provisioning Your System With TLH's Dotfiles                           '
	@echo '======================================================================='
	@echo '                                                                       '
	@echo 'Introduction:                                                          '
	@echo '                                                                       '
	@echo '   The repository you have downloaded features the dotfiles of Thomas  '
	@echo '   Leon Highbaugh. These are opinionated configurations TLH uses in    '
	@echo '   both professional and personal settings which have been configured  '
	@echo '   in an understandable way that provides users with a basis for deep  '
	@echo '   personalization of their pacman-based Linux installations, commonly '
	@echo '   referred to ricing an install. While suitable for use as-is, it is  '
	@echo '   that users instead take some time to modify these files to personal '
	@echo '   personal taste and, of course, YMMV.                                '
	@echo '                                                                       '
	@echo '======================================================================='
	@echo 'Usage:                                                                 '
	@echo '                                                                       '
	@echo '    To provision your system, combine the `make` command with any of   '
	@echo '    of the provisioning candidate commands. This will begin to run the '
	@echo '    specific provisioning commands listed under that system`s command. '
	@echo '                                                                       '
	@echo '======================================================================='
	@echo '                                                                       '
	@echo 'Examples:                                                              '
	@echo '                                                                       '
	@echo '   | Command       | Expected Results                              |   '
	@echo '   |---------------|-----------------------------------------------|   '
	@echo '   | make laptop   | Provision system using `laptop` system group  |   '
	@echo '   | make browsers | Only provision system with `browsers` command |   '
	@echo '                                                                       '
	@echo '======================================================================='
	@echo ' 								      '
	@echo '                                                                       '
	@echo 'System Groups:                                                         '
	@echo '                                                                       '
	@echo '   This makefile is designed to work within specific contexts, based   '
	@echo '   upon the systems utilized by TLH within his own network and thus    '
	@echo '   contain configurations and packages specific to hardware of an      '
	@echo '   idiosyncratic nature that derives from their components. Again, it  '
	@echo '   is best if users first examine and modify this Makefile and scripts '
	@echo '   which this Makefile calls before provisioning locally. The systems  '
	@echo '   listed below are those contexts which this Makefile was designed to '
	@echo '   provision are called following the make command in BASH or ZSH when '
	@echo '   in the directory of this repository.                                '
	@echo '                                                                       '
	@echo '   NOTE: Only laptop is presently configured, others will follow as    '
	@echo '         I can replace the hardware and/or I am able to power systems  '
	@echo '         presently in storage while my living situation stabilizes.    '
	@echo '         - TLH 03/10/2020                                              '
	@echo '                                                                       '
	@echo ' | System Type      | command | System Specs (requirements)          | '
	@echo ' |-------------------------------------------------------------------| '
	@echo ' | Laptop           | laptop  | AMD Ryzen 5 (> 2nd gen)              | '
	@echo ' | Desktop          | desktop | amd64 Intel systems ( > 4th gen)     | '
	@echo ' | Virtual Machines | virt      | pacman-based OS  (2 Cores + 4GB RAM) | '
	@echo ' | Raspberry Pi     | pi      | > raspberry pi 2B                    | '
	@echo '                                                                       '
	@echo '======================================================================='
	@echo 'Specific Provisioning Commands:                                        '
	@echo '                                                                       '
	@echo '   Each of the commands run by the system provisioning command is      '
	@echo '   a set of files which will be linked to the user`s home directory in '
	@echo '   locations which will be seen by the applications they configure as  '
	@echo '   well as installation scripts which install the applications and any '
	@echo '   additional libraries, applications which might be needed in the use '
	@echo '   of the title application or application category. Due to preference '
	@echo '   no minimalism is employed by these scripts and applications are not '
	@echo '   only chosen due to sheer necessity but instead by potential for use.'
	@echo '                                                                       '
	@echo '   Each of these commands could also be run following make if users    '
	@echo '   only want to provision their systems with those configurations.     '
	@echo '                                                                       '
	@echo ' | Command  | Functionality Provided                                  |'
	@echo ' |----------|---------------------------------------------------------|'
	@echo ' | sh         | - System Wide Shell Configuration                     |'
	@echo ' | zsh        | - Default Shell                                       |'
	@echo ' | bash       | - Consistent Backup Shell                             |'
	@echo ' | rc         | - Misc Configurations                                 |'
	@echo ' | uefi       | - Firmware Updater                                    |'
	@echo ' | kitty      | - Terminal Emulator                                   |'
	@echo ' | ryzen      | - AMD Laptop Tools                                    |'
	@echo ' | awesome    | - Default Window Manager                              |'
	@echo ' | archive    | - File Compression Tools and Formats                  |'
	@echo ' | disk       | - HDD/SSD Formatting Tools and Drivers                |'
	@echo ' | X          | - X Server Resources and Modifications                |'
	@echo ' | tmux       | - Terminal Multiplexer                                |'
	@echo ' | vim        | - Console-based Text Editor                           |'
	@echo ' | neovim     | - Console and GUI Text Editor                         |'
	@echo ' | rofi       | - Application Selector and Script Wrapper             |'
	@echo ' | browsers   | - Web Browsers w/System-wide Firefox Modification     |'
	@echo ' | docker     | - Containerization                                    |'
	@echo ' | dev        | - Web Development Environment Provisioning            |'
	@echo ' | emacs      | - Spacemacs (Emacs + VIM keybindings)                 |'
	@echo ' | fonts      | - System Fonts Used By Other Configurations)          |'
	@echo ' | git        | - Version Control                                     |'
	@echo ' | gtk        | - Desktop Window Theme Engine                         |'
	@echo ' | android    | - Android Development and Phone Access                |'
	@echo ' | media      | - Media Playback                                      |'
	@echo ' | netsec     | - System Security Enhancements                        |'
	@echo ' | virt       | - Virtual Machine Emulation                           |'
	@echo ' | minikube   | - Cluster Emulation                                   |'
	@echo ' | postgresql | - Database Emulation                                  |'
	@echo ' | pip        | - Python Package Installation                         |'
	@echo ' | yarn       | - Node Package Installation                           |'
	@echo '                                                                       '

all: sh zsh bash rc uefi kitty awesome zathura laptop neofetch archive disk X tmux vim neovim rofi browsers docker dev emacs fonts git gtk android media netsec virt minikube postgresql pip yarn

laptop: sh zsh bash rc uefi kitty awesome zathura  laptop neofetch  archive disk X tmux vim i3 neovim rofi browsers docker dev emacs fonts git gtk android media virt minikube postgresql pip yarn

vm: sh zsh bash rc kitty awesome archive zathura neofetch disk X tmux vim neovim rofi browsers docker dev emacs fonts git gtk netsec pip yarn

mini: sh zsh bash rc kitty awesome i3 archive zathura neofetch disk X tmux vim neovim rofi browsers docker dev emacs fonts git gtk netsec pip yarn android media vm

desktop:sh zsh bash rc kitty awesome zathura neofetch archive disk X tmux vim neovim rofi browsers docker dev emacs fonts git gtk android media netsec nvidia virt minikube postgresql pip yarn

pi: sh zsh bash rc uefi awesome archive zathura  neofetch disk X tmux vim neovim rofi browsers dev emacs fonts git gtk media netsec pip yarn

android:
	@echo 'Installing Android Packages'
	sh ${PWD}/lib/install/android.sh
archive:
	@echo 'Installing Filesystem Archive Packages'
	sh ${PWD}/lib/install/archive.sh
	sudo systemctl enable snapd
	sudo systemctl start snapd
awesome:
	@echo 'Installing Awesome Packages'
	sh ${PWD}/lib/install/awesome.sh
	yay -S --needed --sudoloop  awesome-git
	@echo 'Installing Awesome Configuration'
	test -L ${HOME}/.config/awesome||  sudo rm -rf ${HOME}/.config/awesome
	sudo mkdir -p ${HOME}/.config/awesome
	sudo ln -sf ${PWD}/awesome/rc.lua ${HOME}/.config/awesome/rc.lua
	sudo ln -sf ${PWD}/awesome/configuration ${HOME}/.config/awesome/configuration
	sudo ln -sf ${PWD}/awesome/widgets ${HOME}/.config/awesome/widgets
	sudo ln -sf ${PWD}/awesome/autorun.sh ${HOME}/.config/awesome/autorun.sh
	sudo ln -sf ${PWD}/awesome/themes ${HOME}/.config/awesome/themes
	sudo ln -sf ${PWD}/awesome/profile.png ${HOME}/.config/awesome/profile.png
bash:
	@echo 'Installing Bash Packages'
	sh ${PWD}/lib/install/bash.sh
	@echo 'Installing Bash Configuration'
	sudo ln -fs ${PWD}/bash/bashrc ${HOME}/.bashrc
	sudo ln -fs ${PWD}/bash/bashenv ${HOME}/.bashenv
	sudo ln -fs ${PWD}/bash/bashprofile ${HOME}/.bash_profile
	sudo ln -fs ${HOME}/.aliases ${HOME}/.bashalias
browsers:
	@echo 'Installing Browser Configuration'
	sh ${PWD}/lib/install/browsers.sh
	test -L ${HOME}/startpage || sudo rm -rf ${HOME}/startpage
	git clone https://github.com/Thomashighbaugh/startpage ${HOME}/startpage
	sudo cp -rvf ${PWD}/firefox/autoconfig.cfg /usr/lib/firefox/defaults/pref/autoconfig.cfg
	sudo cp -rvf ${PWD}/firefox/autoconfig.js /usr/lib/firefox/defaults/pref/autoconfig.js
	sudo cp -rvf ${PWD}/firefox/autoconfig.cfg /usr/lib/firefox-developer-edition/defaults/pref/autoconfig.cfg
	sudo cp -rvf ${PWD}/firefox/autoconfig.js /usr/lib/firefox-developer-edition/defaults/pref/autoconfig.js
	@echo "To install cutomized firefox, create a chrome directory in each profile and populate it with userChrome and friends."
dev:
	@echo 'Installing Development Environment Packages'
	sh ${PWD}/lib/install/dev.sh 
disk:
	@echo 'Installing Filesystem Packages'
	sh ${PWD}/lib/install/disk.sh 
	@echo 'Install File Manager Configuration'
	mkdir -p ${HOME}/.config/pcmanfm
	sudo ln -svf ${PWD}/pcmanfm ${HOME}/.config/pcmanfm
	sudo pacman -S --noconfirm ranger 
	test -L ${HOME}/.config/ranger || rm -rf ${HOME}/.config/ranger
	mkdir -p ${HOME}/.config/ranger
	sudo ln -svf ${PWD}/rc/rc.conf ${HOME}/.config/ranger/rc.conf
desktop:
	@echo 'Installing Desktop Specific Configurations'
	sh ${PWD}/lib/install/desktop.sh
docker:
	@echo 'Installing Docker Configuration'
	sh ${PWD}/lib/install/docker.sh 
	@echo 'Installing Docker Configuration'
	sudo usermod -aG docker ${USER}
	sudo systemctl enable docker.service
	sudo systemctl start docker.socket
	sudo systemctl enable docker.socket
	sudo systemctl start docker.service
	sudo ln -svf ${PWD}/docker ${HOME}/docker
emacs:
	@echo 'Installing Spacemacs'
	sh ${PWD}/lib/install/emacs.sh 
	sudo ln -svf ${PWD}/emacs/spacemacs ${HOME}/.spacemacs
	test -L ${HOME}/.emacs.d || rm -rf ${HOME}/.emacs.d
	git clone https://github.com/syl20bnr/spacemacs ${HOME}/.emacs.d
	git clone https://github.com/borgnix/spacemacs-journal.git ~/.emacs.d/private/journal
	git clone https://github.com/evacchi/tabbar-layer ~/.emacs.d/private/tabbar
fonts:
	@echo 'Installing Font Packages'
	sh ${PWD}/lib/install/fonts.sh 
	sudo mkdir -p ${HOME}/.local/share/fonts
	sudo cp -rv ${PWD}/fonts/fonts.tar.7z ${HOME}/.local/share/fonts
	sudo 7z x -so ${HOME}/.local/share/fonts/fonts.tar.7z | sudo tar xf - -C ${HOME}/.local/share/fonts
	sudo cp -rnv ${HOME}/.local/share/fonts /usr/share/fonts
	sudo fc-cache -vf && fc-cache -vf 
git:
	@echo 'Installing Git Onfiguration'
	sudo ln -fs ${PWD}/git/gitconfig ${HOME}/.gitconfig
	sudo ln -fs ${PWD}/git/gitignore ${HOME}/.gitignore
	sudo ln -fs ${PWD}/git/gitcommit ${HOME}/.gitcommit 
gtk:
	@echo 'Installing GTK Configuration'
	sh ${PWD}/lib/install/gtk.sh
	mkdir -p ${HOME}/.config/gtk-2.0
	sudo ln -svf ${PWD}/gtk/gtk-2.0/gtkfilechooser.ini ${HOME}/.config/gtk-2.0/gtkfilechooser.ini
	mkdir -p ${HOME}/.config/gtk-3.0
	sudo ln -svf ${PWD}/gtk/gtk-3.0/bookmarks ${HOME}/.config/gtk-3.0/bookmarks
	sudo ln -svf ${PWD}/gtk/gtk-3.0/gtk.css ${HOME}/.config/gtk-3.0/gtk.css
	sudo ln -svf ${PWD}/gtk/gtk-3.0/settings.ini ${HOME}/.config/gtk-3.0/settings.ini
	sudo ln -svf ${PWD}/gtk/gtkrc-2.0 ${HOME}/.gtkrc-2.0
	mkdir -p ${HOME}/.config/Kvantum
	sudo ln -svf ${PWD}/gtk/kvantum.kvconfig ${HOME}/.config/Kvantum/kvantum.kvconfig
	mkdir -p ${HOME}/.themes
	test -L ${HOME}/.themes/Dhumavati-Theme || rm -rf ${HOME}/.themes/Dhumavati-Theme 
	git clone https://github.com/Thomashighbaugh/Dhumavati-Theme ${HOME}/.themes/Dhumavati-Theme
	sh ${HOME}/.themes/Dhumavati-Theme/Install
	wget -qO- https://git.io/fhQdI | sh
	sudo suru-plus-folders -C aurora --theme Suru++
	sudo ln -sfv ${PWD}/gtk/gtk-bookmarks ${HOME}/.gtk-bookmarks
	sudo cp -rvf ${PWD}/awesome/profile.png ${HOME}/.face
	sudo cp -rvf ${PWD}/awesome/profile.png /usr/share/backgrounds/profile.png
	@echio 'Setting Grub Theme'
	test -L ${HOME}/Bhairava-Grub-Theme || sudo rm -rf ${HOME}/Bhairava-Grub-Theme
	git clone https://github.com/Thomashighbaugh/Bhairava-Grub-Theme ${HOME}/Bhairava-Grub-Theme
	sudo sh ${HOME}/Bhairava-Grub-Theme/svg2png.sh 
	sudo sh ${HOME}/Bhairava-Grub-Theme/set-grub.sh 
i3:
	@echo 'Installing i3 Window Manager Configuration'
	sh ${PWD}/lib/install/i3.sh
	mkdir -p ${HOME}/.config/i3
	sudo ln -svf ${PWD}/i3/config ${HOME}/.config/i3/config
	sudo ln -svf ${PWD}/i3/i3blocks.conf ${HOME}/.config/i3/i3blocks.conf
	sudo ln -svf ${PWD}/polybar ${HOME}/.config/polybar
	sudo ln -svf ${PWD}/i3/autoi3 ${HOME}/.config/i3
	git clone https://github.com/Thomashighbaugh/Vice-Base16 ${HOME}/Vice-Base16
	mkdir ${HOME}/.backgrounds
	cp -rvf ${HOME}/Vice-Base16/vice.png ${HOME}/.backgrounds
kitty:
	@echo 'Installing Terminal Emulator'
	sh ${PWD}/lib/install/kitty.sh
	test -L ${HOME}/.config/kitty || rm -rf ${HOME}/.config/kitty
	mkdir -p ${HOME}/.config/kitty
	sudo ln -sf ${PWD}/kitty/kitty.conf ${HOME}/.config/kitty/kitty.conf
	sudo ln -sf ${PWD}/kitty/theme.conf ${HOME}/.config/kitty/theme.conf
laptop:
	@echo "Configuring Your ThinkPad"
	sh ${PWD}/lib/install/laptop.sh
	sudo systemctl enable tlp.service
	sudo systemctl enable acpid
media:
	@echo 'Installing Media Packages'
	sh ${PWD}/lib/install/media.sh
	yay -S --needed --noconfirm gpick coulr  
minikube:
	@echo 'Installing Minikube Packages'
	sh ${PWD}/lib/install/minikube.sh
	sudo usermod -a -G libvirt ${USER}
	sudo systemctl start libvirtd.service
	sudo systemctl enable libvirtd.service
	sudo systemctl start virtlogd.service
	sudo systemctl enable virtlogd.service
	minikube config set vm-driver kvm2
neofetch:
	test -L ${HOME}/.config/neofetch || rm -rf ${HOME}/.config/neofetch
	mkdir -p ${HOME}/.config/neofetch
	sudo ln -svf ${PWD}/rc/neofetch.config ${HOME}/.config/neofetch/config.conf
neovim:
	@echo 'Installing Neovim Configuration'
	sh ${PWD}/lib/install/nvim.sh
	mkdir -p ${HOME}/.config/nvim
	sudo ln -svf ${PWD}/nvim/init.vim ${HOME}/.config/nvim/init.vim
	sudo ln -svf ${PWD}/nvim/nvim.desktop /usr/share/applications/nvim.desktop
nvidia:
	@echo 'Installing Nvidia Drivers and Packages'
	sh ${PWD}/lib/install/nvidia.sh
pip:
	@echo 'Installing Python Packages'
	mkdir -p ${HOME}/.local
	curl https://pyenv.run | bash
	curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
	python get-pip.py --user
	@[ -f ${HOME}/.pyenv ] && rm -r ${HOME}/.pyenv && echo "Clearing PyEnv" || echo "No PyEnv Proceeding"
	sh ${PWD}/lib/install/pip.sh
rc:
	@echo 'Install Runtime Configurations'
	@echo 'Install Pacman Configuration Globally'
	sudo ln -svf ${PWD}/rc/pacman.conf /etc/pacman.conf
	@echo "Install Runtime Configuration Packages"
	sh ${PWD}/lib/install/rc.sh
	@echo "Runtime Configurations"
	sudo ln -svf ${PWD}/rc/ackrc ${HOME}/.ackrc
	sudo ln -svf ${PWD}/rc/appimagelauncher.cfg ${HOME}/.config/appimagelauncher.cfg
	sudo ln -svf ${PWD}/rc/picom.conf ${HOME}/.config/picom.conf
	sudo ln -svf ${PWD}/rc/dmrc ${HOME}/.dmrc
	test -L ${HOME}/.config/dunst || rm -rf ${HOME}/.config/dunst
	mkdir -p ${HOME}/.config/dunst
	sudo ln -svf ${PWD}/rc/nanorc ${HOME}/.nanorc
	sudo systemctl enable --now fstrim.timer
	sudo systemctl start fstrim.service

rofi:
	@echo 'Install Rofi Configuration'
	sh ${PWD}/lib/install/rofi.sh
	sudo ln -svf ${PWD}/rofi ${HOME}/.config/
ryzen:
	@echo 'Installing Laptop Tools'
	sh ${PWD}/lib/install/ryzen.sh
sh:
	@echo 'Install Generic Shell Configuration'
	sudo ln -fns ${PWD}/bin/ ${HOME}/bin
	sudo ln -fs ${PWD}/lib/ ${HOME}/lib
	sudo ln -fs ${PWD}/sh/aliases ${HOME}/.aliases
	sudo ln -fs ${PWD}/sh/profile ${HOME}/.profile
	sudo ln -vsf ${DOTFILES}/rc/xterm.desktop /usr/share/applications/xterm.desktop
	sudo ln -vsf ${DOTFILES}/rc/uxterm.desktop /usr/share/applications/uxterm.desktop
tmux:
	@echo 'Install TMUX Configuration'
	sh ${PWD}/lib/install/tmux.sh
	mkdir -p ${HOME}/.tmuxp
	sudo ln -fs ${PWD}/tmux/tmux.conf ${HOME}/.tmux.conf
	sudo ln -vsf ${PWD}/tmux/tmuxp/main.yml ${HOME}/.tmuxp/main.yml
	git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
uefi:
##@echo 'Update Firmware'
##sh ${PWD}/lib/install/uefi.sh
##sudo dmidecode -s bios-version
##fwupdmgr refresh
##fwupdmgr get-updates
##fwupdmgr update
vim:
	@echo 'Install VIM configuration'
	sh ${PWD}/lib/install/vim.sh
	mkdir -p ${HOME}/.vim 
	sudo ln -sv ${PWD}/vim/vimrc ${HOME}/.vimrc
	sudo ln -svf ${PWD}/vim ${HOME}/.vim
	sudo ln -sv ${PWD}/vim/vim/plugins.vim ${HOME}/.vim/plugins.vim
	sudo ln -svf ${PWD}/vim/vim/plugged ${HOME}/.vim/plugged
	sudo ln -sv ${PWD}/vim/vim/autoload ${HOME}/.vim/autoload

virt:
	@echo 'Install Virtual Machine Packages'
	sh ${PWD}/lib/install/vm.sh
X:
	@echo 'Install X Server Configuration'
	sh ${PWD}/lib/install/x.sh
	sudo ln -svf ${PWD}/rc/Xresources ${HOME}/.Xresources
	mkdir -p ${HOME}/.Xresources.d
	sudo ln -svf ${PWD}/X/color ${HOME}/.Xresources.d/color
	sudo ln -svf ${PWD}/X/font ${HOME}/.Xresources.d/font
	sudo ln -svf ${PWD}/X/rxvt-unicode ${HOME}/.Xresources.d/rxvt-unicode
	sudo ln -svf ${PWD}/X/uxterm ${HOME}/.Xresources.d/uxterm
	sudo ln -svf ${PWD}/X/xterm ${HOME}/.Xresources.d/xterm
	sudo ln -fs ${PWD}/X/xinitrc ${HOME}/.xinitrc
	sudo ln -fs ${PWD}/X/xprofile ${HOME}/.xprofile
	xrdb ~/.Xresources
yarn:
	@echo 'Install Node Packages'
	sh ${PWD}/lib/install/yarn.sh
zathura:
	sh ${PWD}/lib/install/zathura.sh
	test -L ${HOME}/.config/zathura || rm -rf ${HOME}/.config/zathura
	mkdir -p ${HOME}/.config/zathura
	sudo ln -svf ${PWD}/zathura/zathurarc ${HOME}/.config/zathura/zathurarc
zsh:
	@echo 'Install ZSH Configuration'
	sh ${PWD}/lib/install/zsh.sh
	curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh
	yay -S --noconfirm --needed zplug powerline 
	sudo ln -fs ${PWD}/zsh/zshrc ${HOME}/.zshrc
	sudo ln -fs ${PWD}/zsh/zlogout ${HOME}/.zlogout
	sudo ln -fs ${PWD}/zsh/zshenv ${HOME}/.zshenv
	sudo ln -sf ${PWD}/zsh/zprofile ${HOME}/.zprofile



